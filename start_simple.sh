#! /usr/bin/env bash
#OAR -p neowise
#OAR -t exotic

set -x

eval "$(guix time-machine -C channels.locked.scm -q -- shell -m manifest.scm --search-paths)"

mpirun -n 4 python3 simple.py

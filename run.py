import subprocess
import os
import time
import signal

os.environ["OMPI_MCA_btl"] = "^openib"
os.environ["OMPI_MCA_osc"]="^ucx"
os.environ["OMPI_MCA_pml"]="^ucx"
os.environ["SstVerbose"] = "3"

nb_sims = 4
reader_ranks = 4
transports = ["rdma", "fabric", "evpath", "wan"]
sst_transport = "fabric"
reader_command = f"mpirun -n {reader_ranks} --oversubscribe -- python3 sstReader.py {nb_sims} {sst_transport}"


def writer_command(i):
    return f"mpirun -n 1 -- python3 sstWriter.py {i} {reader_ranks} {sst_transport}"


print(f"Reader command: {reader_command}", end="\n\n")
reader_process = subprocess.Popen(reader_command, shell=True,
                                  preexec_fn=os.setsid)
st = time.time()
try:
    procs = []
    for i in range(nb_sims):
        cmd = writer_command(i)
        procs.append(subprocess.Popen(cmd, shell=True,
                                      preexec_fn=os.setsid))
        print(f"Writer {i} command: {cmd}")
    for i in range(nb_sims):
        procs[i].wait()
except KeyboardInterrupt:
    print("Keyboard interrupt detected, terminating reader process...")
    os.killpg(os.getpgid(reader_process.pid), signal.SIGTERM)
    reader_process.wait()
    for i in range(nb_sims):
        os.killpg(os.getpgid(procs[i].pid), signal.SIGTERM)
        procs[i].wait()
finally:
    import glob
    time.sleep(2)
    for f in glob.glob("*.sst"):
        os.remove(f)


end = time.time()
print(f"\n\nReaders and writer have finished in ~{end - st: .2f} seconds")

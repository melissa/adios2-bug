;; Devshell:
;; $ guix time-machine -C ./channels-locked.scm -q -- shell -m ./manifest.scm --no-grafts
;;
;; Profile:
;; $ guix time-machine -C ./channels-locked.scm -q -- package -p ./profile -m ./manifest.scm --no-grafts
;; $ GUIX_PROFILE="$PWD/profile"
;; $ . "$GUIX_PROFILE/etc/profile"

;; --no-grafts is needed because of a bug on tensorflow / guix-science, which will cause the package to be recompiled
;; instead of being pulled from source

;; channels-locked.scm is used to not have to configure ~/.config/guix/channels.scm and to use the
;; specific channels versions for which tensorflow is cached

(use-modules (guix packages)
             (guix-hpc packages utils))

(define* (without pkg dep)
  (package
    (inherit pkg)
    (inputs (modify-inputs (package-inputs pkg)
              (delete dep)))))

(concatenate-manifests
  (list
    (package->development-manifest
      (specification->package "py-melissa-core"))
    (package->development-manifest
      (specification->package "melissa-api"))
    (packages->manifest 
      (list (without adios2 "ucx")))
    (specifications->manifest
      (list 
        "bash"
        ;; "adios2"
        "libfabric@1.19.0"
        "openmpi@4"))))

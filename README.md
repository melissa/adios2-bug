# Bug recreation

This repo consists of examples from the Adios2 repo. I have modified the files such that we can create multiple readers and writers based on the `nb_sims` (number of simulation).

Simply run the `python3 run.py` that will run the processes reader and writer via mpirun calls.


## Writer
Writer takes the argument simulation_id and will create engine on `helloSst-{sim_id}` file as well as the number of reader to wait for `RendezvousReaderCount: server_ranks`

## Reader
The reader is a bit different. Instead of creating separate MPI processes for each file, we invoke each reader inside `nb_sims` number of threads. Thus, for each rank, we have separate threads on separate engines.

# Error
The reader will go in blocked state indefinitely. If `nb_sims > 1` and `reader ranks > 1`. It is hard to find where the block is happening.

# With respect to MELISSA
We can have several server (reader) ranks which run two threads
- engine scanner thread that is checking whether a new engine has opened on a file
- main thread doing the data reception until all engines stop sending the data

In this repo, the reader is doing both engine opening as well as data reception. But, the problem seems to be very similar such that both situation end up blocking somewhere.

Also, it is not always that this scenario occurs in our project. For example in our configurations,
```
parameter_sweep_size=30
server_ranks=2
server_logs_verbosity=2
```
The same blocking scenario can occur. But, if we increase the verbosity to 5 (DEBUG). The program runs successfully. This is probably related to some sort of synchronization that is happening due to the delay of printing logs.

Similarily, if we increase the `server_ranks=12` or something, the program will run successfully.

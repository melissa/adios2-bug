#! /usr/bin/env bash
#OAR -l /nodes=1/core=4,walltime=00:02:00
#OAR --stdout stdout
#OAR --stderr stderr
#OAR -t exotic
#OAR -p neowise

set -x

eval "$(guix time-machine -C channels.locked.scm -q -- shell -m manifest.scm --search-paths)"

python3 run.py

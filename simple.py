from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# Perform some work before the barrier (optional)

comm.Barrier()

# Perform some work after the barrier

if rank == 0:
  print("All processes are synchronized!")

import sys
from mpi4py import MPI
import numpy as np
from time import sleep
import adios2.bindings as adios2


def run(sim_id, reader_ranks, sst_transport):
    # MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    # ADIOS MPI Communicator
    adios = adios2.ADIOS(comm)

    # ADIOS IO
    sstIO = adios.DeclareIO("myIO")
    sstIO.SetEngine("Sst")

    sstIO.SetParameters({
        "DataTransport": sst_transport,
        "StepDistributionMode": "AllToAll",
        "RendezvousReaderCount": str(reader_ranks)
    })
    myArray = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                       dtype=np.float32)
    myArray = 10.0 * rank + myArray
    nx = len(myArray)
    increment = nx * size * 1.0

    # ADIOS Variable
    ioArray = sstIO.DefineVariable(
        "bpFloats", myArray, [size * nx], [rank * nx], [nx],
        adios2.ConstantDims
    )

    sstWriter = sstIO.Open(f"helloSst-{sim_id}", adios2.Mode.Write)
    for i in range(4):
        sstWriter.BeginStep()
        sstWriter.Put(ioArray, myArray, adios2.Mode.Sync)
        myArray += increment
        print(f"writer={rank}\tsim-id={sim_id}\ttimestep={sstWriter.CurrentStep()}")
        # Warning: the data is not published until EndStep is called
        sstWriter.EndStep()
        sleep(1.0)

    sstWriter.Close()


if __name__ == "__main__":

    sim_id = 1 if len(sys.argv) == 1 else int(sys.argv[1])
    reader_ranks = 1 if len(sys.argv) == 1 else int(sys.argv[2])
    sst_transport = "evpath" if len(sys.argv) == 1 else str(sys.argv[3])
    print(f"Running the writer with sim_id={sim_id}, "
          f"and readers' count={reader_ranks}")
    run(sim_id, reader_ranks, sst_transport)

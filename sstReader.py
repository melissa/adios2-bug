import sys
from mpi4py import MPI
import numpy as np
import adios2.bindings as adios2
from threading import Thread, Timer


def run(nb_sims, comm, sst_transport):

    # just for printing and distribution of set_selection
    rank = comm.Get_rank()
    size = comm.Get_size()
    counter = 0
    # Adios objects not shared amongst reader ranks. So, it is COMM_SELF
    adios = adios2.ADIOS()
    sstReaders = {}

    # First opening all the engines that are waiting
    for i in range(nb_sims):
        # ADIOS IO
        sstIO = adios.DeclareIO(f"myIO-{i}")
        sstIO.SetEngine("Sst")
        sstIO.SetParameter("DataTransport", sst_transport)
        sstReaders[i] = (sstIO.Open(f"helloSst-{i}", adios2.Mode.Read),
                            sstIO)

    # Going through each time step in the order of engines.
    while counter < len(sstReaders):
        for sim_id, (sstReader, sstIO) in sstReaders.items():
            status = sstReader.BeginStep()
            current_step = sstReader.CurrentStep()
            print(f"reader={rank}\tsim-id={sim_id}\ttimestep={current_step}\tstatus={status}")
            if status == adios2.StepStatus.OK:
                var = sstIO.InquireVariable("bpFloats")
                shape = var.Shape()
                count = int(shape[0] / size)
                start = count * rank
                if rank == size - 1:
                    count += shape[0] % size
                var.SetSelection([[start], [count]])
                floatArray = np.zeros(count, dtype=np.float32)
                sstReader.Get(var, floatArray)
                sstReader.EndStep()
                print(f"reader={rank}\tsim-id={sim_id}\ttimestep={current_step}\t"
                    f"received nnz={np.count_nonzero(floatArray)}.")
            elif status == adios2.StepStatus.EndOfStream:
                sstReader.Close()
                counter += 1
                print(f"reader={rank}\tsim-id={sim_id} finished.")


def some_computation():
    import math
    total = 0.
    for i in range(10000):
        total += i ** 2


if __name__ == "__main__":

    nb_sims = 1 if len(sys.argv) == 1 else int(sys.argv[1])
    sst_transport = "evpath" if len(sys.argv) == 1 else str(sys.argv[2])
    comm = MPI.COMM_WORLD
    run(nb_sims, comm, sst_transport)
